# $ activate tf_gpu
from _VGG19 import _VGG19
from BN_VGG19 import BN_VGG19
from Dropout_VGG19 import Dropout_VGG19
from _MobileNet import _MobileNet
from BN_MobileNet import BN_MobileNet
from Dropout_MobileNet import Dropout_MobileNet
from keras.applications import imagenet_utils
from keras.applications.inception_v3 import preprocess_input
from keras.utils import HDF5Matrix
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping
from keras.models import Sequential
from keras.layers import Dense, Flatten, Activation
from keras import backend as K
K.clear_session()
K.tensorflow_backend._get_available_gpus()
from skimage.transform import resize
import numpy as np
import time
import argparse
from dataAugmentation import *
import cv2
import random
import json
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

def dataAugmentation(img, dataAug):
    #if none
    if dataAug is None :
        return img
        
    #random operation
    if dataAug == 'random':
        opt = ['pixel','transform','global']
        idx = np.random.randint(0,high=len(opt))
        dataAug = opt[idx]
            
    #global    
    if dataAug == 'global':
        opt = ['spnoise','luminosity','contrast']
        op = np.random.randint(0,high=len(opt))
        if opt[op] == 'spnoise':
            return spnoise(img)
        if opt[op] == 'luminosity':
            return luminosity(img)
        if opt[op] == 'contrast':
            return contrast(img)
    
    #pixel 
    if dataAug == 'pixel':
        opt = [1,10,100,1000]
        op = np.random.randint(0,high=len(opt))
        return pixel(img,opt[op])
        
    #transform
    if dataAug == 'transform':
        opt = ['rotation','mirror']
        op = np.random.randint(0,high=len(opt))
        if opt[op] == 'rotation':
            return rotation(img)
        if opt[op] == 'mirror':
            return mirror(img)
    return img

def generateArrayFromHDF5(pathx,pathy, dataSize, batch_size, inputShape,dataAug=None):
    while(1):
        x = np.zeros((0, inputShape[0], inputShape[1],3))
        y = np.zeros((0,1))
        for i in random.sample(range(dataSize), batch_size):
        
            #load images
            tmp = np.asarray(HDF5Matrix(pathx, 'x', start=i, end=i+1))[0]      
                
            #apply dataAugmentation
            prob = random.uniform(0.0,1.0)
            if(prob < 0.5):
                tmp = np.asarray([dataAugmentation(tmp,dataAug)])
            else :
                tmp = np.asarray([tmp])
                                
            #load labels and add to batch         
            x = np.append(x,tmp, axis=0)
            tmp = np.asarray(HDF5Matrix(pathy, 'y', start=i, end=i+1))  
            tmp = np.reshape(tmp, (1,1))   
            y = np.append(y,tmp, axis=0)
        yield (x, y)
        
def trainAndEvaluate(name, Network, inputShape,dataug = None, earlyStop = True):            
    # load model
    print("[INFO] loading "+name+"...")
    model = Network(include_top = False, weights=None, input_shape=inputShape)
    model.compile(optimizer='sgd', loss='binary_crossentropy', metrics=['accuracy'])
    model.summary()

    # init
    train_size      = 262000
    testValid_size  = 32500
    batch_size      = 20

    # create generator
    train_generator = generateArrayFromHDF5('data/camelyonpatch_level_2_split_train_x.h5',
                                            'data/camelyonpatch_level_2_split_train_y.h5',
                                            train_size,batch_size,inputShape,dataAug=dataug)
    test_generator  = generateArrayFromHDF5('data/camelyonpatch_level_2_split_test_x.h5' ,
                                            'data/camelyonpatch_level_2_split_test_y.h5',
                                            testValid_size,batch_size,inputShape)
    valid_generator = generateArrayFromHDF5('data/camelyonpatch_level_2_split_valid_x.h5',
                                            'data/camelyonpatch_level_2_split_valid_y.h5',
                                            testValid_size,batch_size,inputShape)
                
    # train
    print("Training...")
    earlyStopper = EarlyStopping(monitor='val_loss',
                                  min_delta=0,
                                  patience=10,
                                  verbose=0, mode='auto')                                 
    start = time.clock()  
    history = model.fit_generator(train_generator,
                        steps_per_epoch = 400,
                        validation_steps = 200,
                        epochs = 210,
                        verbose=1, 
                        validation_data=valid_generator, 
                        callbacks = [earlyStopper] if earlyStop else [])                            
    model.save('models/'+name+".h5")
    elapsed = time.clock() 
            
    # get learning curves
    acc         = np.asarray(history.history['acc'])
    val_acc     = np.asarray(history.history['val_acc'])
    gen_error   = acc-val_acc
    
    #save data
    job = {'acc':acc.tolist(),'val_acc':val_acc.tolist(),'gen_error':gen_error.tolist()}
    with open('output/'+name+'.json', 'w+', encoding='utf-8') as outfile:
        json.dump(job, outfile) 
    
    #plot
    plt.plot(acc, color='r')
    plt.plot(val_acc, color='b')
    plt.ylabel('acc')
    plt.savefig('figures/'+name+"_acc.png")
    plt.close()
             
    with open('output/'+name+'_out.txt', 'w+') as f:
        print("time taken : ",(elapsed - start)/60 , " min", file=f)
        
        # evaluate                        
        start = time.clock()  
        print("Evaluate...")   
        accuracy = []
        for i in range(10):
            print(i,'/',10)
            scoreSeg = model.evaluate_generator(test_generator,steps = 3250)
            accuracy.append(scoreSeg[1])
        accuracy = np.asarray(accuracy)
        elapsed = time.clock() 
        print("Time to evaluate : ",(elapsed - start)/60," min")
        print("Accuracy (avg, std) = ("+str(accuracy.mean())+","+str(accuracy.std())+")", file=f)
        print("Training Accuracy & Validation Accuracy & Generlization error = ("+str(acc[-1])+","+str(val_acc[-1])+","+str(acc[-1]-accuracy.mean())+")", file=f)

#TRAIN MODELS
trainAndEvaluate("MobileNet",_MobileNet, (96, 96, 3), earlyStop = False) 
trainAndEvaluate("VGG19",_VGG19, (96, 96 ,3), earlyStop = False) 

trainAndEvaluate("BN_MobileNet",BN_MobileNet, (96, 96, 3), earlyStop = False) 
trainAndEvaluate("Dropout_MobileNet",Dropout_MobileNet, (96, 96, 3), earlyStop = False) 
trainAndEvaluate("BN_VGG19",BN_VGG19, (96, 96 ,3), earlyStop = False) 
trainAndEvaluate("Dropout_VGG19",Dropout_VGG19, (96, 96 ,3), earlyStop = False) 

trainAndEvaluate("DArandom_MobileNet",BN_MobileNet, (96, 96, 3),dataug='random', earlyStop = False) 
trainAndEvaluate("DAglobal_MobileNet",BN_MobileNet, (96, 96, 3),dataug='global', earlyStop = False) 
trainAndEvaluate("DApixel_MobileNet",BN_MobileNet, (96, 96, 3),dataug='pixel', earlyStop = False) 
trainAndEvaluate("DAtransform_MobileNet",BN_MobileNet, (96, 96, 3),dataug='transform', earlyStop = False) 
trainAndEvaluate("DArandom_VGG19",_VGG19, (96, 96, 3),dataug='random', earlyStop = False) 
trainAndEvaluate("DAglobal_VGG19",_VGG19, (96, 96, 3),dataug='global', earlyStop = False) 
trainAndEvaluate("DApixel_VGG19",_VGG19, (96, 96, 3),dataug='pixel', earlyStop = False) 
trainAndEvaluate("DAtransform_VGG19",_VGG19, (96, 96, 3),dataug='transform', earlyStop = False) 
                    
                    