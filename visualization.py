import _MobileNet
import matplotlib
matplotlib.use('agg')
from vis.utils import utils
from vis.visualization import visualize_saliency, overlay, visualize_cam, visualize_activation
from matplotlib import pyplot as plt
import numpy as np
import matplotlib.cm as cm
from keras import activations
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img
from keras.utils import HDF5Matrix
from vis.input_modifiers import Jitter
from keras.models import load_model
from keras.utils.generic_utils import CustomObjectScope


def visualize(name, img, model):
    img = np.expand_dims(img, axis=0)
    print(model.predict(img))
    
    #visualize_saliency
    print('visualize_saliency')
    for bm in ['guided']:
        for gm in [None]:
            print('None' if gm is None else gm," ",'None' if bm is None else bm)
            grads = visualize_saliency(model, -1, filter_indices=0, seed_input=img, backprop_modifier=bm,grad_modifier=gm)
            
            plt.imshow(img[0])
            plt.imshow(grads, cmap='jet', alpha = 0.8)
            plt.savefig("viz/"+name+"_visualize_saliency"+"_"+('None' if gm is None else gm)+"_"+('None' if bm is None else bm)+".png")
            plt.show()
    
    #visualize_activation
    print('visualize_activation')
    for bm in [None]:
        for gm in [None]:
            print('None' if gm is None else gm," ",'None' if bm is None else bm)
            grads = visualize_activation(model, -1, filter_indices=0, max_iter=1000,tv_weight=0.,input_modifiers=[Jitter(0.03)], backprop_modifier=bm,grad_modifier=gm)
            
            #plt.imshow(img[0])
            plt.imshow(grads)
            plt.savefig("viz/"+name+"_visualize_activation"+"_"+('None' if gm is None else gm)+"_"+('None' if bm is None else bm)+"_Jitter.png")
            plt.show()
    
     
    #visualize_cam
    print('visualize_cam')
    for bm in ['guided']:
        for gm in [None]:
            print('None' if gm is None else gm," ",'None' if bm is None else bm)
            grads = visualize_cam(model, -1, filter_indices=0, seed_input=img, backprop_modifier=bm,grad_modifier=gm) 
            
            plt.imshow(img[0])
            plt.imshow(grads,cmap='jet', alpha = 0.8)
            plt.savefig("viz/"+name+"_visualize_cam"+"_"+('None' if gm is None else gm)+"_"+('None' if bm is None else bm)+".png")
            plt.show()

            
names = ["VGG19","MobileNet","BN_MobileNet","Dropout_MobileNet","BN_VGG19",
        "Dropout_VGG19","DArandom_MobileNet","DAglobal_MobileNet","DApixel_MobileNet",
        "DAtransform_MobileNet","DArandom_VGG19","DAglobal_VGG19","DApixel_VGG19",
        "DAtransform_VGG19"]
        
for name in names:
    #load model
    if("MobileNet" in name):
        with CustomObjectScope({'relu6': _MobileNet.relu6,'DepthwiseConv2D': _MobileNet.DepthwiseConv2D}):
            model = load_model('models/'+name+'.h5')
    else:
        model = load_model('models/'+name+'.h5')
    
    #load image
    selected = 113
    img = np.asarray(HDF5Matrix("data/camelyonpatch_level_2_split_test_x.h5", 'x', start=selected, end=selected+1))
    y = np.asarray(HDF5Matrix("data/camelyonpatch_level_2_split_test_y.h5", 'y', start=selected, end=selected+1))    
    
    #save image
    plt.imshow(img[0])
    plt.savefig("viz/"+name+"_vizImage.png")
    plt.show()
    
    #visualize
    visualize(name,img[0],model)
