# import the necessary packages
# $ activate tf_gpu
import _VGG19
import _MobileNet
from keras.applications import imagenet_utils
from keras.applications.inception_v3 import preprocess_input
from keras.utils import HDF5Matrix
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping
from keras.models import Sequential
from keras.layers import Dense, Flatten, Activation
from skimage.transform import resize
import numpy as np
import time
import argparse
from dataAugmentation import *
import cv2
import random
import json
from keras import backend as K
K.clear_session()
K.tensorflow_backend._get_available_gpus()
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.metrics import confusion_matrix
from keras.utils.generic_utils import CustomObjectScope
from keras.models import load_model

def generateArrayFromHDF5(pathx,pathy, dataSize, batch_size, inputShape,dataAug=None):
    for j in range(0,dataSize,batch_size):  
        x = np.zeros((0, inputShape[0], inputShape[1],3))
        y = np.zeros((0,1))
        for i in range(j,j+batch_size):
        
            #load images
            tmp = np.asarray(HDF5Matrix(pathx, 'x', start=i, end=i+1))
                                
            #load labels and add to batch         
            x = np.append(x,tmp, axis=0)
            tmp = np.asarray(HDF5Matrix(pathy, 'y', start=i, end=i+1))  
            tmp = np.reshape(tmp, (1,1))   
            y = np.append(y,tmp, axis=0)
        yield (x, y)
        
def produceLearningCurve(name):
    print("produceLearningCurve")
    #load json
    with open("output/"+name+".json", 'r', encoding='utf-8') as f:
        try:
            data = json.load(f)
        except ValueError:
            print("ERROR")
    
    #create plots and save    
    plt.plot(data['acc'], color='r')
    plt.plot(data['val_acc'], color='b')
    plt.ylabel('accuracy')
    plt.savefig('figures/'+name+"_acc.png")
    plt.close()
    
    plt.plot(data['gen_error'], color='g')
    plt.ylabel('generalization error')
    plt.savefig('figures/'+name+"_gen_error.png")
    plt.close()
    
def produceProductionMatrix(y_act,Y_pred,name):
    print("produceProductionMatrix")
    #output to file
    with open("output/CM_"+name+".txt", 'w', encoding='utf-8') as f:
        print(confusion_matrix(y_act, Y_pred), file=f)

    
def ProduceROCCurve(y_act,Y_pred,name):
    print("ProduceROCCurve")
    
    #compute ROC and AUC
    fpr_keras, tpr_keras, thresholds_keras = roc_curve(y_act, Y_pred)
    auc_keras = auc(fpr_keras, tpr_keras)
    
    #create plot and save 
    plt.figure(1)
    plt.plot([0, 1], [0, 1], 'k--')
    plt.plot(fpr_keras, tpr_keras, label='Keras (area = {:.3f})'.format(auc_keras))
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.title('ROC curve')    
    plt.legend()
    plt.savefig('figures/'+name+"_ROC.png")
    plt.show()
    plt.close()
    
def outputBestAndWorst(y_act,Y_pred,name,model):
    print("outputBestAndWorst")
    
    #find best and worst
    min = 1
    max = 0
    best = 0
    worst = 0
    for i in range(len(y_act)):
        loss = (Y_pred[i]-y_act[i])**2
        if(loss < min):
            min = loss
            best = i
        if(loss > max):
            max = loss
            worst = i
    
    #load images
    best_img = np.asarray(HDF5Matrix("data/camelyonpatch_level_2_split_test_x.h5", 'x', start=worst, end=worst+1))
    worst_img = np.asarray(HDF5Matrix("data/camelyonpatch_level_2_split_test_x.h5", 'x', start=best, end=best+1))
            
    #save images
    plt.imshow(best_img[0])
    plt.savefig("bestAndWorst/"+str(best)+"_"+name+"_"+str(Y_pred[best])+"_"+str(y_act[best])+"_"+"best.png")
    plt.show()
    plt.close()
    
    plt.imshow(worst_img[0])
    plt.savefig("bestAndWorst/"+str(worst)+"_"+name+"_"+str(Y_pred[worst])+"_"+str(y_act[worst])+"_"+"_worst.png")
    plt.show()
    plt.close()

def probToclass(y_prob):
    y_class = []
    for y in y_prob:
        y = 1 if y >= 0.5 else 0
        y_class.append(y)
    return np.asarray(y_class)
    

names = ["VGG19","MobileNet","BN_MobileNet","Dropout_MobileNet","BN_VGG19",
        "Dropout_VGG19","DArandom_MobileNet","DAglobal_MobileNet","DApixel_MobileNet",
        "DAtransform_MobileNet","DArandom_VGG19","DAglobal_VGG19","DApixel_VGG19",
        "DAtransform_VGG19"]
        
for name in names:
    # load model
    print("Load model")
    if("MobileNet" in name):
        with CustomObjectScope({'relu6': _MobileNet.relu6,'DepthwiseConv2D': _MobileNet.DepthwiseConv2D}):
            model = load_model('models/'+name+'.h5')
    else:
        model = load_model('models/'+name+'.h5')
    
    # make prediction
    print("make prediction")
    test_size = 32500
    batch_size = 100
    test_generator = generateArrayFromHDF5('data/camelyonpatch_level_2_split_test_x.h5','data/camelyonpatch_level_2_split_test_y.h5',test_size,batch_size,(96, 96 ,3))        
    Y_pred = []
    Y_act = []
    for i,batch in enumerate(test_generator):
        print(i,"/",test_size//batch_size)
        Y_pred.extend(model.predict(batch[0]).ravel())
        Y_act.extend(batch[1].ravel())
    Y_act = np.asarray(Y_act).ravel()
    Y_pred = np.asarray(Y_pred).ravel()
    print("size :", len(Y_act))
    print("size :", len(Y_pred))
    
    # produce insight
    print("produce insight")
    produceLearningCurve(name)
    produceProductionMatrix(Y_act,probToclass(Y_pred),name)
    ProduceROCCurve(Y_act,Y_pred,name)
    outputBestAndWorst(Y_act,Y_pred,name,model)
