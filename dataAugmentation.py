from keras.applications import imagenet_utils
from keras.applications.inception_v3 import preprocess_input
from keras.utils import HDF5Matrix
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping
from keras.models import Sequential
from keras.layers import Dense, Flatten, Activation
from skimage.transform import resize
from vis.utils import utils
import numpy as np
import time
import argparse
import cv2
import random
from keras import backend as K
K.clear_session()
K.tensorflow_backend._get_available_gpus()
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

def mirror(img):
    op = np.random.randint(0,high=2)
    return cv2.flip( img, op)
    
def rotation(img):
    a,b,c = img.shape
    opt = [90,180,270]
    op = np.random.randint(0,high=len(opt))
    M = cv2.getRotationMatrix2D((a/2,b/2),opt[op],1)
    return cv2.warpAffine(img,M,(a,b))
    
def pixel(img,nb):
    pixToRem = np.random.choice(img.shape[0]*img.shape[1], nb, replace=False)
    for a in pixToRem:
        x = a//img.shape[1]
        y = a%img.shape[1]
        img[x,y,0] = 0
        img[x,y,1] = 0
        img[x,y,2] = 0
    return img
    
def contrast(img):
    beta = np.random.randint(0,high=50)
    alpha = 1
    return cv2.convertScaleAbs(img, alpha=alpha, beta=beta)    
    
def luminosity(img):
    alpha = random.uniform(1.0,2.0)
    beta = 0
    return cv2.convertScaleAbs(img, alpha=alpha, beta=beta)  
    
def spnoise(img):
    prob = random.uniform(0.0,0.1)
    output = np.zeros(img.shape,np.uint8)
    thres = 1 - prob 
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            rdn = random.random()
            if rdn < prob:
                output[i][j] = 0
            elif rdn > thres:
                output[i][j] = 255
            else:
                output[i][j] = img[i][j]
    return output
