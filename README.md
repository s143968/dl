DeepLearning
------------

We want to compare the 

    accuracy | printed after training
        -average and variance
    speed of convergence | Look at the graph
        -training error
        -validation error
    solution (using visualization)    
        -what part does the model focus on    
        -using https://raghakot.github.io/keras-vis/
    generalization error | Look at the graph & output
        -Tr vs Val vs Ts error
    learning curve | Look at the graph
        -shape

of multiple models such as 

    vgg16
    vgg19
    nasnetlarge
    densenet121
    mobilenet
    inceptionresnetv2
    inception
    xception
    resnet50

and analyze the effect of common overfitting prevention technique such as 

    Data augmentation
    Regularizer
    Dropout
    Mini-batch

and maybe different learning algorithm such as

    SGD
    RMSprop
    Adagrad
    Adadelta
    Adam
    Adamax
    Nadam
    
Before running the code you need to install tensorflow and download the data
1) Download all the files from :

    https://drive.google.com/drive/folders/1gHou49cA1s5vua2V5L98Lt8TiWA3FrKB

2) Run this command to create a new conda tensorflow environement

    conda create --name tf tensorflow

3) Activate the environement

    activate tf        
    
4) You can run the script as 

    test.py 
    or
    test.py --model <name>
    