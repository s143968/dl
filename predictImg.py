import _VGG19
import _MobileNet
from keras.applications import imagenet_utils
from keras.applications.inception_v3 import preprocess_input
from keras.utils import HDF5Matrix
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping
from keras.models import Sequential
from keras.layers import Dense, Flatten, Activation
from skimage.transform import resize
import numpy as np
import time
import argparse
from dataAugmentation import *
import cv2
import random
import json
from keras import backend as K
K.clear_session()
K.tensorflow_backend._get_available_gpus()
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.metrics import confusion_matrix
from keras.utils.generic_utils import CustomObjectScope
from keras.models import load_model


names = ["VGG19","MobileNet","BN_MobileNet","Dropout_MobileNet","BN_VGG19",
        "Dropout_VGG19","DArandom_MobileNet","DAglobal_MobileNet","DApixel_MobileNet",
        "DAtransform_MobileNet","DArandom_VGG19","DAglobal_VGG19","DApixel_VGG19",
        "DAtransform_VGG19"]
        
#load image
selected = 20322
worstVGG = np.asarray(HDF5Matrix("data/camelyonpatch_level_2_split_test_x.h5", 'x', start=selected, end=selected+1))
y_worst  = np.asarray(HDF5Matrix("data/camelyonpatch_level_2_split_test_y.h5", 'y', start=selected, end=selected+1))  
print("actual : ",y_worst.ravel()[0]) 
plt.imshow(worstVGG[0])
plt.savefig("worst.png")
plt.show()
plt.close()

selected = 26672
bestVGG  = np.asarray(HDF5Matrix("data/camelyonpatch_level_2_split_test_x.h5", 'x', start=selected, end=selected+1))
y_best   = np.asarray(HDF5Matrix("data/camelyonpatch_level_2_split_test_y.h5", 'y', start=selected, end=selected+1))  
print("actual : ",y_best.ravel()[0]) 
plt.imshow(bestVGG[0])
plt.savefig("best.png")
plt.show()
plt.close()

for name in names:
    # load model
    print("Load model")
    if("MobileNet" in name):
        with CustomObjectScope({'relu6': _MobileNet.relu6,'DepthwiseConv2D': _MobileNet.DepthwiseConv2D}):
            model = load_model('models/'+name+'.h5')
    else:
        model = load_model('models/'+name+'.h5')
         
    # make prediction
    print(name," worst : ", model.predict(worstVGG).ravel()[0])
    print(name," best  : ", model.predict(bestVGG).ravel()[0])
    